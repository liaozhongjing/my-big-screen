const { defineConfig } = require('@vue/cli-service')
const path = require("path")
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      scss: {    
        // v10 以后的写法
        additionalData: `@import "@/assets/css/theme.scss";`
      }
    }
  }
})
