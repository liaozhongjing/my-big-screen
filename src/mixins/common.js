import Box from '@/components/Box'
import MyEcharts from '@/components/MyEcharts'

export default {
  components: {
    Box,
    MyEcharts
  }
}
