//注意：不建议使用该方式实现局部全屏，用于该框架才采用缩放的方式实现大屏自适应，使用该方式可能会遇到不可预估的问题。可考虑使用路由切换的方式去模拟全屏效果
//局部全局功能的混入,挂载时需要先调用initRote  使用时调用 fullscreen 方法，并将要全屏切换的元素选择器传进去
export default {
    data() {
        return {
            isFullscreen: false,
            rote: 1, //用于全屏之后对元素的缩放
            element: null, //缩放元素
        }
    },
    watch: {
        isFullscreen() {
            console.log(this.element,this.rote);
            let children = this.element.children
            if (children.length == 0) return
            // `scale(${1},${1})`
            let transformValue = this.isFullscreen ? `scale(${this.rote},${this.rote})` : `unset`
            for (let i = 0; i < children.length; i++) {
                const element = children[i];
                element.style.transform = transformValue;
                element.style.transformOrigin = 'left top';
            }

            // let echarts = this.element.querySelector('.echarts')
            // if(!echarts) return
            // echarts.style.transform = transformValue
        },
    },
    mounted() {
    },
    beforeDestroy() {
        this.element && this.element.removeEventListener("fullscreenchange", this.fullscreenchange);
    },
    methods: {
        fullscreen(str) {
            let element = document.querySelector(str);
            if (!element) return;
            // 浏览器兼容
            if (this.isFullscreen) {
                // 取消全屏
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
            } else {
                // 全屏
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.webkitRequestFullScreen) {
                    element.webkitRequestFullScreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }
        },
        fullscreenchange(e) {
            if (document.fullscreenElement) {
                this.isFullscreen = true;
            } else {
                this.isFullscreen = false;
            }
        },
        initRote(str) {
            this.$nextTick(() => {
                let element = document.querySelector(str);
                this.element = element
                this.element && element.addEventListener("fullscreenchange",this.fullscreenchange)
                let originW = element.clientWidth;
                let originH = element.clientHeight;
                const bodyWidth = document.body.clientWidth;
                const bodyHeight = document.body.clientHeight;
                const scaleX = bodyWidth / originW;
                const scaleY = bodyHeight / originH;
                this.rote = scaleX > scaleY ? scaleY : scaleX;
                console.log(this.rote, scaleX, scaleY);
            })

        }
    }
}