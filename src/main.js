import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/echarts'
import "@/assets/css/base.css"
import commonMixin from "@/mixins/common"

Vue.config.productionTip = false
Vue.mixin(commonMixin)

new Vue({
  router,
  render: h => h(App),
  store,
}).$mount('#app')
