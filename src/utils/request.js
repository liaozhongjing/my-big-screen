// import axios from 'axios'
// import { config, cookie } from '@/utils'

// axios.defaults.timeout = 100000;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'//'application/json; charset=utf-8'; //'application/x-www-form-urlencoded';  
// axios.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest'

// // 添加请求拦截器
// axios.interceptors.request.use((req) => {
//     let token=localStorage.getItem('token');
//     if(req.params instanceof FormData || req.data instanceof FormData) {
//         if (token && req.method == 'post')req.data.append('token',token)
//         if (token && req.method == 'get')req.params.append('token',token)
//         return req
//     } 
//     let usertype = Number(localStorage.getItem('usertype'))
//     let current_company_id = usertype==3 ? Number(localStorage.getItem('current_company_id')) : undefined;
//     if (token && req.method == 'post') req.data = { token:token,current_company_id, usertype, ...req.data }
//     if (token && req.method == 'get') req.params = { token: token,current_company_id,usertype, ...req.params }
//     return req;
// }, (error) => {
//     return Promise.reject(error);
// });

// // 添加响应拦截器
// axios.interceptors.response.use((response) => {
//     if (response.status == 200) {
//         let hire_invitei = window.location.href.indexOf('hire_invite') !== -1 //入职邀请
//         let registration_form = window.location.href.indexOf('registration_form') !== -1 //入职登记表
//         if (response.data.status_code == 99999 && !hire_invitei && !registration_form) {    // 登录过期
//             let params = cookie.getUrlAllParams();
//             let url = window.location.origin;
//             cookie.delCookie()
//             for (let key in params) {
//                 if (key != 'token') url += key + '=' + params[key] + '&';
//             }
//             let href = location.href
//             window.location.href = config.domain+'/acctlogin?referer_url=' + href
//         }
//         if (response.data.status_code == 1) {    // 没有权限或者报错
//             _vue.$Message.destroy()
//             _vue.$Message.error(response.data.status_msg);
//         }
//     }
//     else {
//         _vue.$Message.destroy()
//         _vue.$Message.error({content: code >= 500 ? '服务器发生错误，请稍后再试！' : '请求发生错误，请检查网络是否连接'});
//         return false
//     }
//     // 对响应数据做点什么
//     return response.data;
// }, (error) => {
//     _vue.$Message.destroy()
//     // 判断请求异常信息中是否含有超时timeout字符串
//     if(error.message.includes('timeout')){   
//         _vue.$Message.error({content: '网络超时'});
//         return Promise.reject(error);
//     }
//     // 对响应错误做点什么
//     return Promise.reject(error);
// });

// const install = function(Vue, config = {}) {
//     Vue.prototype.$request = axios
// }

// export {
//     install,
//     axios
// };