import * as echarts from 'echarts/core'
import china from "@/assets/map/china.json";
import {
  BarChart,
  PieChart,
  MapChart,
  EffectScatterChart,
  LineChart,
  ScatterChart 
} from 'echarts/charts'

import {
  TitleComponent,
  TooltipComponent,
  GridComponent,
  VisualMapComponent,
  GeoComponent,
  LegendComponent
} from 'echarts/components'

echarts.use([
  TitleComponent,
  TooltipComponent,
  GridComponent,
  BarChart,
  PieChart,
  VisualMapComponent,
  GeoComponent,
  MapChart,
  EffectScatterChart,
  LineChart,
  LegendComponent,
  ScatterChart 
])
echarts.registerMap("china", china);

