import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 创建仓库 store
const store = new Vuex.Store({
    state:{

    },
    mutations:{

    },
    actions:{
        
    }
})

export default store