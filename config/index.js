
const config = {
   development: {
        ermapi: 'http://erm.service.consul/ermApi', //员工管理接口
        apiurl: 'http://yun.service.consul',
        api_v1: 'http://yun.service.consul/api/v1/',
        yun_domain: 'http://yun.service.consul',
        domain: 'http://yunm.service.consul',
        contract:'http://contract.service.consul', //人事签
        env: 'dev'
    },
    local: {
        ermapi: 'http://erm.service.consul/ermApi',
        apiurl: 'http://yun.service.consul',
        api_v1: 'http://yun.service.consul/api/v1/',
        yun_domain: 'http://yun.service.consul',
        domain: 'http://yunm.service.consul',
        contract:'http://contract.service.consul', //人事签
        env: 'local'
    },
    test: {
        apiurl: 'https://tpa.renshihr.cn',
        api_v1: 'https://tpa.renshihr.cn/api/v1/',
        yun_domain: 'https://tpa.renshihr.cn',
        domain: 'http://renshihr.cn',
        contract:'http://contract.renshihr.cn', //人事签
        env: 'test'
    },
    production: {
        apiurl: 'https://api.renshihr.com',
        api_v1: 'https://api.renshihr.com/api/v1/',
        yun_domain: 'https://api.renshihr.com',
        domain: 'https://www.renshihr.com',
        contract:'https://contract.renshihr.com', //人事签
        env: 'production'
    }
}
console.log(process.env.VUE_APP_CONFIG)
module.exports = config[process.env.VUE_APP_CONFIG || 'development']